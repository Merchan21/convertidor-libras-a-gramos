package facci.ronaldmerchanco.convertidordelibras;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ConvertirActivity extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertir);

        texto = (TextView) findViewById(R.id.textpara);
        String bundle = this.getIntent().getExtras().getString("Libras");
        Double gramos = Double.valueOf(bundle)*453.592;
        texto.setText(gramos.toString());

    }
}